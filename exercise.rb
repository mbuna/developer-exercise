class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # outWords = str.split().map { |w| w.length > 4 ? w.downcase == w ? "marklar" : "Marklar" : w }
    inWords = str.split()
    outWords = []
    for w in inWords
      nw = w
      if w.length > 4
        nw = "marklar"
        if w.capitalize == w
          nw = "Marklar"
        end
        if w.include? "."
          nw << "."
        end
        if w.include? "!"
          nw << "!"
        end
        if w.include? "?"
          nw << "?"
        end
      end
      outWords.push(nw)
    end
    return outWords.join(" ")
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    i = 0
    fib = []
    while i < nth
      if fib.length < 2
        fib.push(1)
      else
        nn = fib.last(2)[0] + fib.last(2)[1]
        fib.push(nn)
      end
      i += 1
    end
    fib.delete_if &:odd?
    return fib.inject(:+)
  end

  if __FILE__==$0
    self.marklar("The quick brown. fox")
    self.marklar("Down goes Frazier")
    # self.marklar("Monday is Martin Luther King day")
    # self.marklar("How is the weather today? I have not been outside.")
    self.even_fibonacci(5)
  end

end
